# Execução do Código-Fonte

## Extração e Transformação de dados
A execução pode ser feita a partir de qualquer diretório do repositório. Esses scripts foram aplicados durante a transformação de dados e não demandam execução para a replicação de análises realizadas partindo dos dados em  _/data/processed/_. Assim, os scripts são apenas descritos:

- match_disease_names_CoV2_HSDN.py
  - verifica a distância de edição entre a lista de doenças da rede HSDN e a lista de doenças relacionadas ao Sars-CoV-2.

- match_disease_names_CoV2_HSDN_scrape_supplementary_concepts.py
  - faz a busca no Pubmed das doenças mapeadas pelos conceitos suplementares e atualiza o respectivo arquivo de dados.

- match_disease_names_CoV2_HSDN_add_tree_categories.py
  - faz uma busca na árvore hierárquica de doenças mapeadas segundo o _MeSH_ e cria um arquivo que fornece a categoria mais ampla (nível 1) a qual cada doença da lista está relacionada.

## Criação e Análise de Redes
As seguintes execuções devem ser feitas na raiz do repositório para replicação da criação e análise das redes:

- Para filtrar a HSDN e a lista proteínas-doenças do CTD, aplicar a métrica de avaliação das conexões da HSDN e agregar os dados de classificação das doenças execute:

`python src/filter_HSDN_matched_disease.py`

- Para criar as redes de proteínas e doenças e de proteínas e classes de doenças execute:

`python src/create_human_diseases_proteins_networks.py`

- Para criar as rede de proteínas e sintomas execute:

`python src/create_human_symptoms_proteins_networks.py`

- Para executar as análises topológicas das redes execute (exemplo para rede SARS-CoV-2-sintomas):

`python src/networks_analysis.py data/processed/human_symptoms_sarscov2_protein_edges_table.csv "Proteínas SARS-CoV-2 e Sintomas Humanas"`
