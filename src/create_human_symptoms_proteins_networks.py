import pandas as pd

hsdn_filtered_file = 'data/processed/hsdn_filtered_edges_table.csv'
hd_protein_file = 'data/processed/human_disease_protein_edges_table.csv'
hd_sarscov2_protein_file = 'data/processed/human_disease_sarscov2_protein_edges_table.csv'

hsdn = pd.read_csv(hsdn_filtered_file)
hsdn.columns = ['MeSH Symptom Term', 'Target', 'PubMed occurrence','TFIDF score', 'association_strength_normstd']
# normalize TFIDF score using max and min
# hsdn['TFIDF score']=(hsdn['TFIDF score']-hsdn['TFIDF score'].min())/(hsdn['TFIDF score'].max()-hsdn['TFIDF score'].min())

# create the human protein symptoms net
hdp = pd.read_csv(hd_protein_file)
# merge the symptoms with the protein by the disease target
hdp_symptom = hdp.merge(hsdn, on='Target')
hdp_symptom.columns = ['Source', 'Disease', 'associated_gene', 'Target','PubMed occurrence', 'TFIDF score', 'Weight']
hdp_symptom.to_csv('data/processed/human_protein_symptoms_edges_table.csv',
                   columns=['Source', 'Target', 'Weight', 'Disease', 'associated_gene', 'PubMed occurrence', 'TFIDF score'],
                   index=False)

print("#Human_protein", len(hdp_symptom.Source.unique()))
print("#Symptoms", len(hdp_symptom.Target.unique()))
print("#Links ", hdp_symptom.shape[0])
# create the human symptoms sarscov2 protein net
hd_sc2p = pd.read_csv(hd_sarscov2_protein_file)
# merge the symptoms with the protein by the disease target
hd_sc2p_symptom = hd_sc2p.merge(hsdn, on='Target')
hd_sc2p_symptom.columns = ['Source', 'Disease', 'Weight_gene', 'Weight_ppi', 'Target', 'PubMed occurrence', 'TFIDF score', 'Weight']
hd_sc2p_symptom['symptom_occurrences'] = 1
# aggregate duplicated rows as a mean weight, if any duplicated source-target
if hd_sc2p_symptom[['Source', 'Target']].duplicated().any():
    hd_sc2p_symptom = hd_sc2p_symptom.groupby(['Source', 'Target'],
                                                        as_index=False).agg({'PubMed occurrence': 'mean',
                                                                             'Weight': 'mean', 'symptom_occurrences': 'sum',
                                                                             'Weight_gene': 'sum', 'Weight_ppi': 'mean',
                                                                             'TFIDF score': 'mean'})


hd_sc2p_symptom.symptom_occurrences = hd_sc2p_symptom.symptom_occurrences/hd_sc2p_symptom.Weight_gene
hd_sc2p_symptom.Weight = hd_sc2p_symptom.Weight*hd_sc2p_symptom.symptom_occurrences

# hd_sc2p_symptom = hd_sc2p_symptom[hd_sc2p_symptom.Weight >= 0.7]
hd_sc2p_symptom.to_csv('data/processed/human_symptoms_sarscov2_protein_edges_table.csv',
                   columns=['Source', 'Target', 'Weight', 'symptom_occurrences', 'Weight_gene', 'Weight_ppi'], index=False)

print("#SARS_COV_2_protein", len(hd_sc2p_symptom.Source.unique()))
print("#Symptoms", len(hd_sc2p_symptom.Target.unique()))
print("#Links ", hd_sc2p_symptom.shape[0])
#import matplotlib.pyplot as plt
# import seaborn as sns
#
# sns.distplot(hd_sc2p_symptom.Weight)