from graph_utils import Network, plot_values_distribution, comparative_fit_plot_ccdf, fit_powerlaw_loglog
import sys
import pandas as pd

network_file= "data/processed/human_disease_protein_edges_table.csv"
network_name = "Proteínas e Doenças Humanas"
if len(sys.argv) >=3:
    network_file = sys.argv[1]
    network_name = sys.argv[2]
else:
    print("Wrong number of parameters. The network file path and the network name must be supplied.")
    sys.exit(1)

net = Network(network_file, network_name)
net.metrics()
n_components = net.info_distance()
degree_list = net.degree_list()
print("Degrees summary: \n", pd.Series(degree_list).describe())

# plot degree and cluster size distributions
plot_values_distribution(degree_list, net.G.number_of_nodes(),
                         title_net_name="Distribuição de Graus da Rede "+network_name,
                         xlim=[1e0, 1e2], ylim=[5e-4, 1e0])
plot_values_distribution(n_components, len(n_components), var_name='n',
                         title_net_name="Distribuição dos Tamanhos dos Componentes da Rede "+network_name,
                         xlim=[1e0, 5e3], ylim=[5e-3, 1e0])

### FIT DISTRIBUTIONS ###
# fit P(n)
net_fit_pn = comparative_fit_plot_ccdf(degree_list= n_components, title_net_name=network_name,
                                       distribution="Distribuição dos Tamanhos dos Componentes da Rede ",
                                       ylabel="P(n)", xlabel="n", ylim=1e-2)
print("p(n)~n^-alpha, alpha = ",net_fit_pn.power_law.alpha)

# fit P(k)
net_fit_pk = comparative_fit_plot_ccdf(degree_list= degree_list, title_net_name=network_name,
                                       distribution="Distribuição de Graus da Rede ")
print("power law fit p(k)~k^-gamma, gamma = ",net_fit_pk.power_law.alpha)
print("truncated power law fit p(k)~C*k^-alpha, alpha = ",net_fit_pk.truncated_power_law.alpha,
      " lambda ", net_fit_pk.truncated_power_law.Lambda)
print("Stretched Exponential fit Beta: ", net_fit_pk.stretched_exponential.beta,"Lambda: ", net_fit_pk.stretched_exponential.Lambda)
# fit Ck
# fit_loglog(np.asarray(degree_list_bra)[np.where(np.asarray(degree_list_bra) > 1)[0]],
#            np.asarray(nodes_C_G_bra)[np.where(np.asarray(degree_list_bra) > 1)[0]],
#            title_="Bra Molecular Networking of Annotations Cluster Coefficient Distribution Fit")
# try_fit_power_law(degree_list, network_name)
