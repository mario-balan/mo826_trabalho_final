import networkx as nx
import pandas as pd
from statistics import mean
import matplotlib.pyplot as plt
import seaborn as sns
import powerlaw as pl

def metrics(graph,net_name=""):
    print('\nNetwork'+net_name+'metrics:')
    print('Number of nodes ', nx.number_of_nodes(graph))
    print('Number of edges ', nx.number_of_edges(graph))
    print('Density ', nx.density(graph))
    print('Number of connected components ', nx.number_connected_components(graph))
    print('Average degree <k> ', 2*nx.number_of_edges(graph)/nx.number_of_nodes(graph))
    distr = nx.degree_histogram(graph)
    print('min k ', [i for i in range(len(distr)) if distr[i] > 0][0])
    print('max k ', len(distr) - 1, [x for x in nx.nodes(graph) if len(list(nx.neighbors(graph, x))) == len(distr)-1])
    print('degree distribution ', distr)
    if nx.is_connected(graph):
        print('avg. dist. ', nx.average_shortest_path_length(graph))
        print('max. dist. ', nx.diameter(graph))
    print('Average clustering coefficient <C> ', nx.average_clustering(graph))
    plt.bar(range(len(distr)),distr)
    plt.xlabel('k', fontsize=18)
    plt.ylabel('N_k', fontsize=16)
    plt.title('Distribuição de Graus da Rede '+net_name)
    plt.show()


class bipartite_network(object):
    # data is a panda csv file with partition nodes in columns 0 and 1
    def __init__(self, data, b_A, b_B):
        # create bipartite graph
        self.G_bi = nx.Graph()
        self.G_bi.add_nodes_from(data.iloc[:, 0].unique(), bipartite=b_A)
        self.G_bi.add_nodes_from(data.iloc[:, 1].unique(), bipartite=b_B)
        if "Weight" in data.columns:
            self.G_bi.add_weighted_edges_from(list(data.itertuples(index=False, name=None)))
        else:
            self.G_bi.add_edges_from(list(data.itertuples(index=False, name=None)))
        # make bipartite projections
        self.A_nodes, self.B_nodes = nx.bipartite.sets(self.G_bi)
        self.G_A, self.G_B = nx.bipartite.weighted_projected_graph(self.G_bi, self.A_nodes), nx.bipartite.weighted_projected_graph(self.G_bi, self.B_nodes)

    def metrics(self):
        print("Bipartite Network")
        print(metrics(self.G_bi))
        print("\nProjection A Network")
        print(metrics(self.G_A))
        print("\nProjection B Network")
        print(metrics(self.G_B))

    def degree_list(self):
        return [[x[1] for x in self.G_bi.degree()],
                [x[1] for x in self.G_A.degree()],
                [x[1] for x in self.G_B.degree()]]

    def projections(self):
        return self.G_A, self.G_B

    def G_bi_export_gephi(self, filename):
        nx.write_gexf(self.G_bi, filename)



class Network(object):
    def __init__(self, data_file, net_name):
        # data is a csv with an edge by row; the first 2 columns are the nodes the 3rd column is the cosine and the 4th is the annotation
        net_data = pd.read_csv(data_file)
        self.name=net_name
        # create undirected network
        self.G = nx.Graph()
        # add not self loops edges
        if "Weight" in net_data.columns:
            self.G.add_weighted_edges_from(list(net_data[['Source', 'Target', 'Weight']].itertuples(index=False, name=None)))
        else:
            self.G.add_edges_from(list(net_data[['Source', 'Target']].itertuples(index=False, name=None)))
    def metrics(self):
        metrics(self.G, self.name)
    def info_distance(self):
        if nx.is_connected(self.G):
            cluster_n = [self.G.number_of_nodes()]
            print('Diameter: ', nx.diameter(self.G), '\nAverage distance: ', nx.average_shortest_path_length(self.G))
        else: # not connected, compute the mean for the components
            comps_shortest_path = []
            comps_diameter = []
            cluster_n = []
            for c in nx.connected_components(self.G):
                cluster_n.append(len(c))
                if len(c) == 1: # do not compute distance for isolated nodes
                    continue
                g = nx.subgraph(self.G, c)
                comps_shortest_path.append(nx.average_shortest_path_length(g))
                comps_diameter.append(nx.diameter(g))
            print('Average Diameter within Components: ', mean(comps_diameter),
                  '\nAverage Distance within Components: ', mean(comps_shortest_path))
        return cluster_n
    def degree_list(self):
        return [x[1] for x in self.G.degree()]
    def connected_components(self):
        components_num = nx.number_connected_components(self.G)
        print('# Connected components: ', components_num)
        return components_num
    def average_clustering(self):
        avg_C = nx.average_clustering(self.G)
        print('Average Clustering: ', avg_C)
        return nx.clustering(self.G)
    def export_gephi(self, filename):
        nx.write_gexf(self.G, filename)

# N can be the size of the list or an idx to be used to aggregate the k_list
def plot_values_distribution(k_list, N, p_name=None, var_name ='k', title_net_name='', xlim=None, ylim=None):
    if type(N) is int:
        k_dist = pd.value_counts(k_list)/N
        p_name = 'p(' + var_name + ')'
        k_dist = pd.DataFrame({var_name: k_dist.index, p_name: k_dist.values})
    else:
        k_dist = pd.DataFrame({var_name: k_list, 'idx': N}).groupby('idx').agg('mean')
        p_name = p_name+'('+var_name+')'
        k_dist = pd.DataFrame({var_name: k_dist.index, p_name: k_dist[var_name]})
    # log-log plot
    sns.set(style="darkgrid")
    g = sns.scatterplot(x=var_name, y=p_name, data=k_dist, sizes=20)
    g.set(xlabel=var_name, ylabel=p_name, title=title_net_name+' Log-Log',
          xscale='log', yscale='log')
    if xlim:
        plt.xlim(xlim[0], xlim[1])
    if ylim:
        plt.ylim(ylim[0], ylim[1])
    plt.show()
    # lin-lin plot
    g = sns.scatterplot(x=var_name, y=p_name, data=k_dist,
                        sizes=20)
    g.set(xlabel=var_name, ylabel=p_name, title=title_net_name+' Lin-lin')
    plt.show()

# Comparative plot fit CCDF
# http://tuvalu.santafe.edu/~aaronc/powerlaws/
# https://nbviewer.jupyter.org/github/jeffalstott/powerlaw/blob/master/manuscript/Manuscript_Code.ipynb
# https://pythonhosted.org/powerlaw/#
def comparative_fit_plot_ccdf(degree_list = None, fit = None, discrete=True, xmin=1.0, title_net_name = '',
                              distribution="Degree Distribution", ylabel="P(k), p(k)≥k", xlabel="k",
                              ylim = 1e-3):
    if not fit:
        if not degree_list:
            print("Need to provide the degree list or the fitted object")
        fit = pl.Fit(degree_list, discrete=discrete,xmin=xmin)
    ####
    fig = fit.plot_ccdf(linewidth=3, label='Dados Experimentais')
    fit.power_law.plot_ccdf(ax=fig, color='b', linestyle='--', label='Power law fit')
    fit.truncated_power_law.plot_ccdf(ax=fig, color='m', linestyle='--', label='Truncated Power law fit')
    fit.lognormal.plot_ccdf(ax=fig, color='g', linestyle='--', label='Lognormal fit')
    fit.lognormal_positive.plot_ccdf(ax=fig, color='c', linestyle='--', label='Lognormal Positive fit')
    fit.exponential.plot_ccdf(ax=fig, color='y', linestyle='--', label='Exponential fit')
    fit.stretched_exponential.plot_ccdf(ax=fig, color='r', linestyle='--', label='Stretched Exponential fit')
    fig.set_ylabel(ylabel, fontsize=16)
    fig.set_xlabel(xlabel, fontsize=18)
    fig.set_title(distribution+title_net_name+" Comparação do Fit da CCDF", fontsize=20)
    handles, labels = fig.get_legend_handles_labels()
    plt.ylim(ylim, 1e0)
    fig.legend(handles, labels, loc=3)
    plt.show()
    print("Kormogorov Smirnov Parameter of the fitted distributions")
    print("Power Law - D:", fit.power_law.D)
    print("Truncated Power Law - D:", fit.truncated_power_law.D)
    print("Lognormal - D:", fit.lognormal.D)
    print("Lognormal Positive - D:", fit.lognormal_positive.D)
    print("Exponential - D:", fit.exponential.D)
    print("Stretched Exponential - D:", fit.stretched_exponential.D)
    return fit

def try_fit_power_law(degree_list, title=''):
    kmax = max(degree_list)
    kmin = min(degree_list)
    fit = pl.Fit(degree_list, discrete=True,xmin=kmin, xmax=kmax)
    D_min = fit.power_law.D
    k_opt = [kmin, kmax]
    for ksat in range(kmin, kmax-1):
        fit = pl.Fit(degree_list, discrete=True, xmin=ksat, xmax=k_opt[1])
        if fit.power_law.D != fit.power_law.D: # if returns nan stop - not enough data
            break
        if fit.power_law.D < D_min:
            D_min = fit.power_law.D
            k_opt[0] = ksat
    for kcut in range(k_opt[0]+1, kmax):
        fit = pl.Fit(degree_list, discrete=True, xmin=k_opt[0], xmax=kcut)
        if fit.power_law.D != fit.power_law.D: # if returns nan stop - not enough data
            break
        if fit.power_law.D < D_min:
            D_min = fit.power_law.D
            k_opt[1] = kcut
    print("\nPower Law best fit")
    print("Opt D:", D_min)
    print("ksat, kcut", k_opt)
    print("Alpha ", fit.power_law.alpha)
    print("")
    fit = pl.Fit(degree_list, discrete=True, xmin=k_opt[0], xmax=k_opt[1])
    # fit = comparative_fit_plot_pdf(fit=fit, title_net_name=title)
    return comparative_fit_plot_ccdf(fit=fit, title_net_name=title)

# Fit Ck
from scipy.optimize import curve_fit
import numpy as np

# fit an exponential function.
# This looks like a line on a lof-log plot.
def myExpFunc(x, b):
    return 1 * np.power(x, -b)

def fit_powerlaw_loglog(x, y, title_):
    fig = plt.figure()
    ax=plt.gca()
    ax.scatter(x,y,c="blue",alpha=0.95,edgecolors='none', label='data')
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlim(1e0,1e2)
    ax.set_ylim(1e-2, 2e0)
    newX = np.logspace(0, 2, base=10)  # Makes a nice domain for the fitted curves.
                                       # Goes from 10^0 to 10^2
                                       # This avoids the sorting and the swarm of lines.
    popt, pcov = curve_fit(myExpFunc, x, y)
    plt.plot(newX, myExpFunc(newX, *popt), 'r-',
             label="(k**-{0:.3f})".format(*popt))
    fig.suptitle(title_, fontsize=20)
    plt.xlabel('k', fontsize=18)
    plt.ylabel('C(k)', fontsize=16)
    print("Exponential Fit: y = (k**-b)")
    print("\ta = popt[0] = {0}".format(*popt))
    ax.grid(b='on')
    plt.legend(loc='lower right')
    plt.show()