import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

def name_checker(wrong_names,correct_names):
    names_array = []
    ratio_array = []
    for wrong_name in wrong_names:
            x=process.extractOne(wrong_name,correct_names,scorer=fuzz.ratio)
            names_array.append(x[0])
            ratio_array.append(x[1])
    return names_array,ratio_array

# read files:
workfile = '../data/external/diseases_associated_to_CoV2_affected_genes.csv'
wordlistfile = '../data/raw/diseases_occurence_terms.csv'

df = pd.read_csv(workfile)
wordlist = pd.read_csv(wordlistfile).DiseaseName.values.tolist()

# match names by Levenshtein Distance ratio:
match_entry = df.DiseaseName.tolist()
match_options = wordlist
name_match,ratio_match=name_checker(match_entry,match_options)

# build an url column to speed up further manual checking:
mesh = 'http://www.ncbi.nlm.nih.gov/mesh/'
omim = 'http://omim.org/entry/'
reference = (df.DiseaseID.str.replace('MESH:', mesh).str.replace('OMIM:', omim)).to_list()



df_out = pd.DataFrame()
df_out['associated_gene'] = df['GeneSymbol']
df_out['correct_ratio'] = pd.Series(ratio_match)
df_out['old_name'] = pd.Series(match_entry)
df_out['correct_name_auto'] = pd.Series(name_match)
df_out['correct_name_manual'] = np.where(df_out.correct_ratio >= 90, '', 'fill')
df_out['check_method'] = np.where(df_out.correct_ratio >= 90, 'Auto', 'Manual')
df_out['reference'] = pd.Series(reference)

df_out.to_csv(r'../data/interim/disease_matching_before_manual_review.csv')
