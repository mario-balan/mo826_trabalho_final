import pandas as pd
import itertools
import csv

# read files:
workfile = '../data/interim/disease_matching_after_manual_review.csv'
referencefile = '../data/interim/2020MeShTreeHierarchy_diseases_only.csv'

with open(referencefile, "r") as csv_file:
    tree = []
    tree = list(csv.DictReader(csv_file, delimiter=','))

df = pd.read_csv(workfile)

df['correct_name_final'] = df.correct_name_auto
df.loc[(df.check_method == 'Manual'),'correct_name_final'] = df.correct_name_manual

disease_names = []
disease_names = list(set(df.correct_name_final.values.tolist()))

interim_tree = []

for name in disease_names:
    for row in tree:
        if (name == row['Term']):
            interim_tree.append([row['Mn'],row['Term']])

for entry in interim_tree:
    for row in tree:
        mns = entry[0].split('.')
        if (mns[0] == row['Mn']):
            del entry[0]
            entry.append(row['Mn'])
            entry.append(row['Term'])

interim_tree.sort()
final_tree = list (interim_tree for interim_tree,_ in itertools.groupby(interim_tree))

final_tree.insert(0,['tree_fullname', 'tree_lvl1_id', 'tree_lvl1_name'])

with open("../data/interim/disease_matching_in_tree_hierarchy.csv", "w", newline="") as output:
    writer = csv.writer(output)
    writer.writerows(final_tree)
