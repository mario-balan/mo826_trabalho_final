import pandas as pd

human_genes_disease_file = 'data/processed/disease_matching_after_manual_review_checked.csv'
ppi_Cov2_human_proteins = 'data/processed/human_sarscov2_ppi_edges_table.csv'

h_genes_data = pd.read_csv(human_genes_disease_file)

# reade PPI data to get the proteins names
PPI_data = pd.read_csv(ppi_Cov2_human_proteins)

# match Gene Symbol with prey Gene and add the respectively SARS-CoV-2 interacting protein
h_genes_data["SAR_CoV_2_protein"] = None
h_genes_data["Human_protein"] = None
h_genes_data["Weight_ppi"] = 0.0
for CoV2_protein, human_protein, human_gene, weight in PPI_data[['Source', 'Target', 'TargetGene', 'Weight']].itertuples(index=False):
    h_genes_data.loc[h_genes_data['associated_gene'] == human_gene,
                     ['SAR_CoV_2_protein', 'Human_protein', 'Weight_ppi']] = [CoV2_protein, human_protein, float(weight)]

del PPI_data

h_genes_data = h_genes_data.sort_values('tree_lvl1_id')
h_genes_data = h_genes_data[['SAR_CoV_2_protein', 'Human_protein', 'correct_name_final', 'associated_gene', 'Weight_ppi', 'tree_lvl1_name']]
h_genes_data.columns = ['SAR_CoV_2_protein', 'Human_protein', 'disease_name', 'associated_gene', 'Weight_ppi', 'disease_class']
# remove duplicates due to different diseases pointing to a more generic one present in the hsdn
h_genes_data = h_genes_data[h_genes_data.duplicated() == False]

sarscov2_disease_net = h_genes_data[['SAR_CoV_2_protein', 'disease_name', 'associated_gene', 'Weight_ppi']]
sarscov2_disease_net = sarscov2_disease_net[sarscov2_disease_net.duplicated() == False] # remove duplated values due to multiple disease class
sarscov2_disease_net = h_genes_data[['SAR_CoV_2_protein', 'disease_name', 'Weight_ppi']]
sarscov2_disease_net.columns = ['Source', 'Target', 'Weight_ppi']
sarscov2_disease_net['Occurrences'] = 1
# aggregate duplicated rows as a mean weight, if any duplicated source-target
if sarscov2_disease_net[['Source', 'Target']].duplicated().any():
    sarscov2_disease_net = sarscov2_disease_net.groupby(['Source', 'Target'],
                                                        as_index=False).agg({'Occurrences': 'sum', 'Weight_ppi': 'mean'})

sarscov2_disease_net.to_csv('data/processed/human_disease_sarscov2_protein_edges_table.csv', index=False)

human_disease_net = h_genes_data[['Human_protein', 'disease_name', 'associated_gene']]
human_disease_net = human_disease_net[human_disease_net.duplicated() == False] # remove duplated values due to multiple disease class
human_disease_net.columns = ['Source', 'Target', 'associated_gene']
human_disease_net.to_csv('data/processed/human_disease_protein_edges_table.csv', index=False)

print("#SAR_CoV_2_protein", len(h_genes_data.SAR_CoV_2_protein.unique()))
print("#Human_protein", len(h_genes_data.Human_protein.unique()))
print("#Disease", len(h_genes_data.disease_name.unique()))
#h_genes_data.to_csv('data/processed/human_disease_protein_sarscov2_protein_data.csv')

# create networks witht he disease's classes
sarscov2_disease_class_net = h_genes_data[['SAR_CoV_2_protein', 'disease_class']]
sarscov2_disease_class_net.columns = ['Source', 'Target']
sarscov2_disease_class_net['Occurrences'] = 1
sarscov2_disease_class_net = sarscov2_disease_class_net.groupby(['Source', 'Target'],
                                                                as_index=False).agg({'Occurrences': 'sum'})
sarscov2_disease_class_net.to_csv('data/processed/human_disease_class_sarscov2_protein_edges_table.csv', index=False)

human_disease_class_net = h_genes_data[['Human_protein', 'disease_class']]
human_disease_class_net.columns = ['Source', 'Target']
human_disease_class_net['Occurrences'] = 1
human_disease_class_net = human_disease_class_net.groupby(['Source', 'Target'],
                                                                as_index=False).agg({'Occurrences': 'sum'})
human_disease_class_net.to_csv('data/processed/human_disease_class_protein_edges_table.csv', index=False)

# save disease class as a label; get the first class if multiple classes
h_genes_data = h_genes_data.reset_index(drop=True)
h_genes_data = h_genes_data[['disease_name','disease_class']]
h_genes_data = h_genes_data.groupby('disease_name', as_index=False).agg('first')  #.agg(lambda col: ';'.join(col))
h_genes_data.columns = ['Id', 'disease_class']
h_genes_data.to_csv('data/processed/diseases_classes_dictionary.csv', index=False)