import csv
import requests
import urllib.request
import time
from bs4 import BeautifulSoup

workfile = '../data/interim/disease_matching_before_manual_review.csv'

# scrape MeSH website to get the diseases mapped by supplementary concepts:
def get_disease_list(mesh):
    try:
        disease_array = []
        #url = 'https://www.ncbi.nlm.nih.gov/mesh/' + mesh
        url = mesh
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        reference = soup.find('p', text='Heading Mapped to:')
        disease_list = reference.find_next('ul').find_all('li')
        for disease in disease_list:
            disease_array.append(disease.text)
        return disease_array
    except:
        print('Failed to scrape for:',mesh)

with open(workfile, "r") as csv_file:
    match_list = []
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    for row in csv_reader:
        if 'mesh/C' in row['reference']:
            match_list.append([row['associated_gene'],row['old_name'],row['reference']])

for match_entry in match_list:
    disease_mapped_headers = get_disease_list(match_entry[2])
    for header in disease_mapped_headers:
        print('"%s","%s","%s","%s"' % (match_entry[0],match_entry[1],header,match_entry[2]))
