import pandas as pd

def norm_std(values, mean_values, std_values):
    return (values-mean_values)/(std_values)

hsdn_file = "data/raw/hsdn_tfidf.txt"
matched_disease_file = "data/interim/disease_matching_after_manual_review.csv"

# get the unique matched disease names
matched_disease = pd.read_csv(matched_disease_file)
# merge direct evidence and filter only marker
diseases_original = pd.read_csv('data/external/diseases_associated_to_CoV2_affected_genes.csv',
                                usecols=['GeneSymbol', 'DiseaseName', 'DirectEvidence'])
diseases_original.columns = ['old_name', 'associated_gene', 'DirectEvidence']
matched_disease = matched_disease.merge(diseases_original, on=['old_name', 'associated_gene'])
matched_disease = matched_disease[matched_disease.DirectEvidence.str.match('marker/mechanism')]

print("* Before match *")
print("Total number of protein gene-disease association:", matched_disease.shape[0])
print("Total number of protein genes:", len(matched_disease.associated_gene.unique()))
print("Total number of diseases:", len(matched_disease.old_name.unique()))

# get correct name from auto and manual fill
matched_disease['correct_name_final'] = matched_disease.correct_name_auto
matched_disease['correct_name_final'][matched_disease.check_method == 'Manual'] = matched_disease.correct_name_manual[matched_disease.check_method == 'Manual']

# match the diseases with their mesh category, cons
disease_class_file = "data/interim/disease_matching_in_tree_hierarchy.csv"
disease_class = pd.read_csv(disease_class_file)
disease_class.columns = ['correct_name_final', 'tree_lvl1_id', 'tree_lvl1_name']
matched_disease_class = matched_disease.merge(disease_class, on='correct_name_final')

# filter the remaining diseases in the hsdn
diseases_names = matched_disease_class.correct_name_final.unique()

# read the human symptons disease network and filter the edges in the diseases names
hsdn = pd.read_csv(hsdn_file, delimiter="\t", skipinitialspace=True)
hsdn = hsdn[(hsdn['MeSH Disease Term'].isin(diseases_names))] # & (hsdn['PubMed occurrence'] > 1)]
# add the diseases and the symptoms total occurrences
disease_occurrences = pd.read_csv('./data/raw/diseases_occurence_terms.txt', delimiter='\t')
disease_occurrences.columns = ['MeSH Disease Term', 'PubMed occurrence disease']
symptoms_occurrences = pd.read_csv('./data/raw/symptoms_occurrence_terms.txt', delimiter='\t')
symptoms_occurrences.columns = ['MeSH Symptom Term', 'PubMed occurrence symptom']
hsdn = hsdn.merge(disease_occurrences, on = 'MeSH Disease Term')
hsdn = hsdn.merge(symptoms_occurrences, on = 'MeSH Symptom Term')
# apply association strength, a probabilistic similarity measure for co-occurrences
# ref https://poseidon01.ssrn.com/delivery.php?ID=675084119021002010098030115125071122056006056079005030120038061112013112023101076102023065091064024126030027012112110023068114000087004068094124030120096126096019118125003070110&EXT=pdf
hsdn['association_strength'] = hsdn['PubMed occurrence']/(hsdn['PubMed occurrence disease']*hsdn['PubMed occurrence symptom'])
hsdn = hsdn.sort_values('association_strength', ascending = False)
hsdn = hsdn.reset_index(drop=True)
# filter the more significant symptomms by disease
# keep only the ones that are above the mean+std
# remove opposite symptoms, keeping the one with more occurrences
# Overweight, Obesity, Weight Gain x Weight Loss, Thinness
# Fever, Hot Flashes, Fever of Unknown Origin x Hypothermia
# Hyperoxia X Hypoxia
# Anorexia x Hyperphagia, Bulimia
# Hypercapnia x Hypocapnia
# Hyperventilation x Hypoventilation
# Muscle Hypertonia x Muscle Hypotonia
opposite_sympts = [('(Overweight|Obesity|Weight Gain)', '(Weight Loss|Thinness)'),
                   ('(Fever|Hot Flashes|Fever of Unknown Origin)', 'Hypothermia'),
                   ('Hyperoxia','Hypoxia'),
                   ('Anorexia','(Hyperphagia|Bulimia)'),
                   ('Hypercapnia', 'Hypocapnia'),
                   ('Hyperventilation', 'Hypoventilation'),
                   ('Muscle Hypertonia', 'Muscle Hypotonia')]
hsdn['association_strength_normstd'] = -1.0
hsdn['significant'] = False
for dis_info in hsdn.groupby('MeSH Disease Term').agg({'association_strength' : ['mean', 'std']}).iterrows():
    dis_pos = (hsdn['MeSH Disease Term'] == dis_info[0])
    hsdn.loc[dis_pos, 'association_strength_normstd'] = norm_std(hsdn.loc[dis_pos, 'association_strength'], dis_info[1][0], dis_info[1][1])
    hsdn.loc[dis_pos, 'significant'] = ((hsdn.loc[dis_pos, 'association_strength_normstd'] > 0.95) &
                                        (hsdn.loc[dis_pos, 'PubMed occurrence'] > 1))
    if sum(hsdn.loc[dis_pos, 'significant']) == 0: # removed all symptons, keep only the first one
        hsdn.loc[dis_pos, 'significant'] = [True]+hsdn.loc[dis_pos, 'significant'][1:].to_list()
        continue
    elif sum(hsdn.loc[dis_pos, 'significant']) == 1:
        continue
    sigf_terms = hsdn[dis_pos].loc[hsdn.loc[dis_pos, 'significant'] == True]
    for proib_symps in opposite_sympts:
        grp1 = sigf_terms['MeSH Symptom Term'].str.match(proib_symps[0])
        # processed to check if group 2 is present,
        # if yes pick the one that have the maximum occurrence
        if grp1.sum() > 0:
            grp2 = sigf_terms['MeSH Symptom Term'].str.match(proib_symps[1])
            if grp2.sum() == 0: continue
            max1 = sigf_terms.loc[grp1, 'PubMed occurrence'].max()
            max2 = sigf_terms.loc[grp2, 'PubMed occurrence'].max()
            if max1 > max2:
                # keep as significant the diseases of group1
                hsdn.loc[sigf_terms.index[grp2], 'significant'] = False
            else: # max2 > max1
                # otherwise keep the diseases of group2
                hsdn.loc[sigf_terms.index[grp1], 'significant'] = False

# remove not sifnificant links between diseases and symptons
hsdn = hsdn[hsdn['significant']]
hsdn.to_csv("data/processed/hsdn_filtered_edges_table.csv", index=False,
            columns = ['MeSH Symptom Term', 'MeSH Disease Term', 'PubMed occurrence','TFIDF score', 'association_strength_normstd'])
print("* HSDN filtered *")
print("#Diseases ", len(hsdn['MeSH Disease Term'].unique()))
print("#Symptons ", len(hsdn['MeSH Symptom Term'].unique()))
print("#Links ", hsdn.shape[0])

# no match disease
not_matched = matched_disease_class.correct_name_final[
        matched_disease_class.correct_name_final.isin(hsdn['MeSH Disease Term'].unique()) == False].unique()
print("* After match *")
if len(not_matched) > 0:
    print("** Diseases not found in the HSDN: **")
    print(not_matched)
    matched_disease_class = matched_disease_class[matched_disease_class.correct_name_final.isin(hsdn['MeSH Disease Term'].unique())]

print("Total number of protein gene-disease association:", matched_disease_class[matched_disease_class[['associated_gene', 'correct_name_final']].duplicated() == False].shape[0])
print("Total number of protein genes:", len(matched_disease_class.associated_gene.unique()))
print("Total number of diseases:", len(matched_disease_class.correct_name_final.unique()))

matched_disease_class.to_csv("data/processed/disease_matching_after_manual_review_checked.csv", index=False)
