import pandas as pd
import networkx as nx
from graph_utils import metrics

raw_PPI_file_path = "./data/interim/ppi_human_sarscov2_disease_sup.csv"

PPI_data = pd.read_csv(raw_PPI_file_path, usecols=["Bait","Preys", "PreyGene","MIST","Saint_BFDR","AvgSpec","FoldChange"])

# remove space from the SARS-CoV-2 proteins name
PPI_data["Bait"] = [x.replace(" ", "_").replace("CoV2", "CoV-2") for x in PPI_data.Bait]

print("Number of SARS-CoV-2 protein baits:", len(PPI_data.Bait.unique()))
print("Number of Human protein preys:", len(PPI_data.Preys.unique()))

G_PPI = nx.Graph()
G_PPI.add_weighted_edges_from(list(PPI_data[['Bait','Preys','MIST']].itertuples(index=False, name=None)))

metrics(G_PPI)

PPI_data = PPI_data[['Bait','Preys','MIST', "PreyGene"]]
PPI_data.columns = ['Source', 'Target', 'Weight', "TargetGene"]
PPI_data.to_csv('data/processed/human_sarscov2_ppi_edges_table.csv', index=False)
#nx.write_gexf(G_PPI, "./networks/PPI_SARS-CoV-2-Human.gexf")
